FROM python:3.7.4
# Create app directory
WORKDIR /srv/altair-client
# Install dependencies
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
# Copy app source code
COPY . .
# Expose listening port
EXPOSE 80
# Runtyme
CMD ["python", "client.py"]