from __future__ import annotations

import json
import logging
import os
import random
import time

import tornado.httpclient
import tornado.ioloop
import tornado.websocket

from src.beer.beer import Beer
from src.helpers.logger import init_logger


class Tap:
    """
    We use a tap class to encapsulate the client functionality. When the program gets run, a new tap is instantiated and
    gets the id from the environment variables set. It exposes a method which is used to open a web socket connection
    between the client and the server and a method used to post beers from the tap. Both these methods are non blocking
    although during the main loop we block the program so the tap doesn't start sending beers until it settles a web
    socket connection with the server
    """

    def __init__(self, _id, inet_address):
        self.__tap_id = _id
        self.__socket_addr = "ws://" + inet_address + "/sockets/beers"
        self.__api_addr = "http://" + inet_address + "/api"
        self.__socket = None

    @property
    def tap_id(self):
        return self.__tap_id

    @property
    def socket(self):
        return self.__socket

    @property
    def socket_addr(self):
        return self.__socket_addr

    @property
    def api_addr(self):
        return self.__api_addr

    @staticmethod
    def notify(message):
        logging.info(message)

    async def open_connection(self):
        """
        The open connection method opens a new WebSocket connection with the server and sets the callback for displaying
        incoming messages.
        :return:
        """
        logging.info('Attempting to connect to: %s', self.socket_addr)
        # Open socket connection on callback mode
        self.__socket = await tornado.websocket.websocket_connect(url=self.socket_addr, on_message_callback=self.notify)

    async def post_beer(self):
        """
        Post Beer: Coroutine that posts beer results to the server
        :return: The request result
        """
        logging.info('Posting beer...')
        volume = random.randint(100, 1000)
        beer = Beer(self.tap_id, volume).serialize()
        result = await client.fetch(
            self.api_addr + "/beers",
            headers={'Content-Type': 'application/json'},
            method='POST',
            body=json.dumps(beer))
        return result


async def main():
    # Create new tap and
    tap = Tap(tap_id, inet_addr)
    # Open webSocket connection
    await tap.open_connection()
    # Start sending beer!
    while True:
        await tap.post_beer()
        time.sleep(random.randint(1, 10))


if __name__ == "__main__":
    # Initialize logger
    init_logger()
    # Get environment variables
    inet_addr = os.environ['INET_ADDR']
    tap_id = os.environ['TAP_ID']
    logging.info("\n\n - Inet Addr: %s \n - Tap ID: %s \n", inet_addr, tap_id)
    # Start Tornado client and main loop
    client = tornado.httpclient.AsyncHTTPClient()
    tornado.ioloop.IOLoop.current().run_sync(main)
